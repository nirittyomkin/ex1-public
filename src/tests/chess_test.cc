#include <gtest/gtest.h>

extern "C" {
    #include "./chess/chessSystem.h"

    #include "./mtm_map/map.h"
}
TEST(Chess, CreateDestroy) {
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    chessDestroy(chess);
}

TEST(Chess, TestAddTournament) {
    GTEST_SKIP_("Not implemented yet");
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    ASSERT_TRUE(CHESS_SUCCESS == chessAddTournament(chess, 1, 1, "a") );
    ASSERT_TRUE(CHESS_INVALID_ID == chessAddTournament(chess, -1, -1, "a!"));
    ASSERT_TRUE(CHESS_TOURNAMENT_ALREADY_EXISTS == chessAddTournament(chess, 1, -1, "a !"));
    ASSERT_TRUE(CHESS_INVALID_LOCATION == chessAddTournament(chess, 2, -1, "a !"));
    ASSERT_TRUE(CHESS_INVALID_MAX_GAMES == chessAddTournament(chess, 1, -1, "abc"));
    chessDestroy(chess);
}

TEST(Chess, TestAddGame) {
    GTEST_SKIP_("Not implemented yet");
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    ASSERT_TRUE(CHESS_SUCCESS == chessAddTournament(chess, 1, 2, "a"));
    ASSERT_TRUE(CHESS_INVALID_ID == chessAddGame(chess, -1, 1, 2, FIRST_PLAYER, -7));
    ASSERT_TRUE(CHESS_INVALID_ID == chessAddGame(chess, 3, 1, 1, FIRST_PLAYER, -7));//This one is for the same id for both players
    ASSERT_TRUE(CHESS_TOURNAMENT_NOT_EXIST == chessAddGame(chess, 2, 1, 2, FIRST_PLAYER, -7));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddGame(chess, 1, 1, 2, FIRST_PLAYER, 7));
    ASSERT_TRUE(CHESS_GAME_ALREADY_EXISTS == chessAddGame(chess, 1, 1, 2, FIRST_PLAYER, -7));
    ASSERT_TRUE(CHESS_INVALID_PLAY_TIME == chessAddGame(chess, 1, 1, 3, FIRST_PLAYER, -7));
    ASSERT_TRUE(CHESS_SUCCESS == chessRemovePlayer(chess, 1));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddGame(chess, 1, 1, 2, FIRST_PLAYER, 7));
    ASSERT_TRUE(CHESS_EXCEEDED_GAMES == chessAddGame(chess, 1, 2, 3, FIRST_PLAYER, -7));
    chessDestroy(chess);
}

TEST(chess, TestRemoveTournament) {
    GTEST_SKIP_("Not implemented yet");
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    ASSERT_TRUE(CHESS_INVALID_ID == chessRemoveTournament(chess, -2));
    ASSERT_TRUE(CHESS_TOURNAMENT_NOT_EXIST == chessRemoveTournament(chess, 1));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddTournament(chess, 1, 2, "a"));
    ASSERT_TRUE(CHESS_SUCCESS == chessRemoveTournament(chess, 1));
    chessDestroy(chess);
}

TEST(chess, TestRemovePlayer) {
    GTEST_SKIP_("Not implemented yet");
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    ASSERT_TRUE(CHESS_INVALID_ID == chessRemovePlayer(chess, -3) );
    ASSERT_TRUE(CHESS_PLAYER_NOT_EXIST == chessRemovePlayer(chess, 1) );
    ASSERT_TRUE(CHESS_SUCCESS == chessAddTournament(chess, 1, 2, "a") );
    ASSERT_TRUE(CHESS_SUCCESS == chessAddGame(chess, 1, 1, 2, FIRST_PLAYER, 7) );
    ASSERT_TRUE(CHESS_SUCCESS == chessRemovePlayer(chess, 1));
    chessDestroy(chess);
}

TEST(chess, TestEndTournament) {
    GTEST_SKIP_("Not implemented yet");
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    ASSERT_TRUE(CHESS_INVALID_ID == chessEndTournament(chess, 0));
    ASSERT_TRUE(CHESS_TOURNAMENT_NOT_EXIST == chessEndTournament(chess, 1));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddTournament(chess, 1, 2, "a"));
    ASSERT_TRUE(CHESS_NO_GAMES == chessEndTournament(chess, 1));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddGame(chess, 1, 1, 2, FIRST_PLAYER, 7));
    ASSERT_TRUE(CHESS_SUCCESS == chessEndTournament(chess, 1));
    ASSERT_TRUE(CHESS_TOURNAMENT_ENDED == chessEndTournament(chess, 1));
    chessDestroy(chess);
}

TEST(chess, TestCalcAvgPlayTime) {
    GTEST_SKIP_("Not implemented yet");
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    ChessResult* error;
    chessCalculateAveragePlayTime(chess, 0, error);
    ASSERT_TRUE(CHESS_INVALID_ID == *error );
    chessCalculateAveragePlayTime(chess, 1, error);
    ASSERT_TRUE(CHESS_PLAYER_NOT_EXIST == *error);
    ASSERT_TRUE(CHESS_SUCCESS == chessAddTournament(chess, 1, 2, "a"));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddGame(chess, 1, 1, 2, FIRST_PLAYER, 7));
    chessCalculateAveragePlayTime(chess, 1, error);
    ASSERT_TRUE(CHESS_SUCCESS == *error);
    chessDestroy(chess);
}

TEST(chess, TestSaveTournamentStatistics) {
    GTEST_SKIP_("Not implemented yet");
    ChessSystem const chess = chessCreate();
    ASSERT_TRUE(chess);
    ASSERT_TRUE(CHESS_NO_TOURNAMENTS_ENDED == chessSaveTournamentStatistics(chess, "/dev/nul"));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddTournament(chess, 1, 2, "a"));
    ASSERT_TRUE(CHESS_SUCCESS == chessAddGame(chess, 1, 1, 2, FIRST_PLAYER, 7));
    ASSERT_TRUE(CHESS_SUCCESS == chessEndTournament(chess, 1));
    ASSERT_TRUE(CHESS_SUCCESS == chessSaveTournamentStatistics(chess, "/dev/nul"));
    chessDestroy(chess);
}

TEST(chess, TestMultipleGamesSamePlayers) {
    GTEST_SKIP_("Not implemented yet");
}
