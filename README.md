# How to use locally:
These tests can either be run from this project, by placing your sources where they're expected to be, or by downloading tests and adding them to your own pr 

For CLion users, you can also run tests using the gui (probably other IDEs can do this too, but I'm only familiar with CLion).


## The recommended method: clone (requires git)
0. Clone:
```bash
$ git clone https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public.git
```
1. Copy your sources to the relevant source directories ([mtm_map](https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public/-/tree/master/mtm_map)] or [chess](https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public/-/tree/master/src/chess))
2. Delete / comment out `./PreLoad.cmake` (unless you prefer ninja, then install ninja-build).
And you're done :)

You may use
* `./build.sh` to build
* `./test.sh` to run all tests
(These are useful in the shell.
If you use an IDE, after it reloads the cmake project it should let you run tests with a click of a button.
If it doesn't, you probably need to learn it better or switch to a different IDE.)


## The manual method: download tests manually (requires following the instructions carefully)
0. Download the desired tests (i.e. [test_map.cc](https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public/-/blob/master/src/tests/map_test.cc)).
1. Correct relative include paths in the downloaded tests.
2. Add the test to your own tests.
3. In your CMakeLists.txt, add the required gtest configuration, as demonstrated in !1:
```cmake
cmake_minimum_required(VERSION 3.18)
# Be sure not to have `project(ex1 C)` (gtest needs C++)
project(ex1)

# Add C++11 standard (for gtest)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_C_STANDARD 99)

# Needed for running tests via cmake
include(CTest)

# Needed for running valgrind on tests automatically
add_custom_target(
        test_memcheck
        COMMAND ${CMAKE_CTEST_COMMAND} --force-new-ctest-process --test-action memcheck
        COMMAND cat "${CMAKE_BINARY_DIR}/Testing/Temporary/MemoryChecker.*.log"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
)

# Needed fot the tests
# GTest instructions here https://google.github.io/googletest/quickstart-cmake.html
include(FetchContent)
FetchContent_Declare(
        googletest
        URL https://github.com/google/googletest/archive/609281088cfefc76f9d0ce82e1ff6c30cc3591e5.zip
)
FetchContent_MakeAvailable(googletest)

enable_testing()

# Add gtest tests here

include(GoogleTest)
```
4. In your CMakeLists.txt, add the tests !1. After "Add gtests tests here":
```cmake
# Add gtest tests here

add_executable(
        map_test
        path/to/your/downloaded/map_test.cc
        path/to/your/own/map.c
)

target_link_libraries(
        map_test
        gtest_main
)
```
5. Then let gtest discover your new test source by adding a `gtest_discover_tests(map_test)` after the existing `include(GoogleTest)`:
```cmake
include(GoogleTest)

gtest_discover_tests(map_test)
```


# Motivation:
1. [Learn in public](https://twitter.com/swyx/status/1009174159690264579?s=19) (the entire thread is highly recommended)
2. [Inspire others to join](https://www.youtube.com/watch?v=fW8amMCVAJQ)


# Requirements:
1. gcc
2. valgrind
3. ninja (optional, disable PreLoad.cmake to use normal "Unix Makefile")


See previous [Merge Requests](https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public/-/merge_requests?scope=all&utf8=%E2%9C%93&state=all&label_name[]=example) to see how to add your own tests for everyone else's benefit.
